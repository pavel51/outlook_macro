Attribute VB_Name = "OutLink"
Sub OutLink_form()
    Dim prefix As String
    prefix = "Outlook"

    Dim olItem As Object
    Dim SelectedItem As String
    Dim linkName As String
    Dim linkID As String
    Dim objName As String
    Dim folderName As String
    Dim description As String
    Dim indent As Boolean
    Dim createDate As String
    Dim author As String
    Dim fName As String
    Dim fDescrip As String
    Dim fID As String
    
    '�������� ���������� ��������� ��������
    If Application.ActiveExplorer.Selection.Count <> 1 Then
        If IsNull(Application.ActiveExplorer.CurrentFolder.EntryID) Then
            MsgBox ("���������� ������� ������ ���� ������")
            Exit Sub
        Else
            fName = Application.ActiveExplorer.CurrentFolder.name
            fDescrip = Application.ActiveExplorer.CurrentFolder.description
            fID = Application.ActiveExplorer.CurrentFolder.EntryID
        End If
    Else
        
        Set olItem = Application.ActiveExplorer.Selection.Item(1)
        SelectedItem = TypeName(olItem)
        
        CaseItem SelectedItem, olItem, linkName, objName, author, createDate, linkID, folderName
        
        fName = Application.ActiveExplorer.CurrentFolder.name
        fDescrip = Application.ActiveExplorer.CurrentFolder.description
        fID = Application.ActiveExplorer.CurrentFolder.EntryID
    
    End If
    
    '������ � ���������� ������
    Call uF.Init(objName, linkName, linkName, author, createDate, fName, fDescrip)
    uF.Show
    Call uF.GetParam(linkName, description, indent)

    If Not indent Then
        prefix = prefix + "-" + objName + ":     "
        '�������� ���������� Word ��������� ��� �������� ������
        CreateLink "outlook:" + linkID, prefix + linkName, description, indent
    Else
        prefix = prefix + "-" + "�����" + folderName + ":     "
        '�������� ���������� Word ��������� ��� �������� ������
        CreateLink "outlook:" + fID, prefix + linkName, description, False
    End If
        
End Sub

Public Sub CaseItem(ByVal SelectedItem As String, ByVal olItem As Object, ByRef linkName As String, _
ByRef objName As String, ByRef author As String, ByRef createDate As String, ByRef linkID As String, ByRef folderName As String)

    folderName = ""
    Select Case SelectedItem
    Case Is = "MailItem"
        linkName = olItem.Subject
        objName = "���������"
        folderName = "(���������)"
        author = olItem.SenderName
        createDate = olItem.ReceivedTime
        linkID = olItem.EntryID
    Case Is = "TaskItem"
        linkName = olItem.Subject
        objName = "������"
        folderName = "(������)"
        author = olItem.Owner
        createDate = olItem.DueDate
        linkID = olItem.EntryID
    Case Is = "NoteItem"
        linkName = olItem.Subject
        objName = "�������"
        folderName = "(�������)"
        createDate = olItem.LastModificationTime
        linkID = olItem.EntryID
    Case Is = "AppointmentItem"
        linkName = olItem.Subject
        objName = "�������"
        folderName = "(�������)"
        createDate = olItem.Start
        linkID = olItem.EntryID
    Case Is = "ContactItem"
        If Replace(olItem.FullName, " ", "") = "" Then
        linkName = olItem.CompanyName
        Else
        linkName = olItem.FullName
        End If
        objName = "�������"
        folderName = "(��������)"
        author = olItem.CompanyName
        createDate = olItem.CreationTime
        linkID = olItem.EntryID
    Case Else
        linkName = ""
        objName = ""
        folderName = ""
        createDate = ""
        linkID = ""
    End Select

End Sub

Public Sub CreateLink(ByVal address As String, ByVal name As String, ByVal descrip As String, _
ByVal indent As Boolean)
    Dim hLink As Object
    Dim appWd As Word.Application
    Dim wdDoc As Word.Document
    
    '�������� ���������� Word ��������� ��� �������� ������
    Set appWd = CreateObject("Word.Application")
    Set wdDoc = appWd.Documents.Add
    appWd.Visible = False
    
    Set hLink = wdDoc.Hyperlinks.Add(Anchor:=wdDoc.Range, _
        address:=address, _
        SubAddress:="", _
        ScreenTip:=descrip, _
        TextToDisplay:=name)
    
    '�������������� ������
    hLink.Range.Font.name = "Segoe UI"
    hLink.Range.Font.Size = 10
    hLink.Range.Font.Color = RGB(0, 0, 255)
    
    If Not indent Then
        '�������� ������
        hLink.Range.Copy
    Else
        appWd.Selection.WholeStory
        With appWd.Selection.ParagraphFormat
            .LeftIndent = CentimetersToPoints(0)
            .RightIndent = CentimetersToPoints(0)
            .SpaceBefore = 0
            .SpaceBeforeAuto = False
            .SpaceAfter = 0
            .SpaceAfterAuto = False
            .LineSpacingRule = wdLineSpaceMultiple
            .LineSpacing = LinesToPoints(0.7)
            .Alignment = wdAlignParagraphLeft
            .WidowControl = True
            .KeepWithNext = False
            .KeepTogether = False
            .PageBreakBefore = False
            .NoLineNumber = False
            .Hyphenation = True
            .FirstLineIndent = CentimetersToPoints(0)
            .OutlineLevel = wdOutlineLevelBodyText
            .CharacterUnitLeftIndent = 0
            .CharacterUnitRightIndent = 0
            .CharacterUnitFirstLineIndent = 0
            .LineUnitBefore = 0
            .LineUnitAfter = 0
            .MirrorIndents = False
            .TextboxTightWrap = wdTightNone
            .CollapsedByDefault = False
        End With

        '�������� ������
        appWd.Selection.Range.Copy
        
    End If
    
    wdDoc.Close (False)
    appWd.Quit
    
    'all done, clean up and close
    Set appWd = Nothing
    Set wdDoc = Nothing
    Set hLink = Nothing
    
End Sub


