VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} uF 
   Caption         =   "������ �������������� ����������"
   ClientHeight    =   2895
   ClientLeft      =   45
   ClientTop       =   330
   ClientWidth     =   13140
   OleObjectBlob   =   "uF.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "uF"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False



Private tmpTheme
Private tmpAuthor
Private tmpDate
Private tmpName
Private tmpDescrip
Private tmpfName
Private tmpfDescrip
Private tmpFlag
Public Sub Init(ByVal objName As String, ByVal name As String, _
ByVal descrip As String, ByVal objAuthor As String, ByVal objDate As String, ByVal fName As String, ByVal fDescrip As String)

objNameLabel.Caption = objName
fNameLabel.Caption = fName
textBox.Text = name
descripBox.Text = ""
tmpDescrip = descrip
tmpTheme = name
tmpFlag = False
tmpfName = fName
tmpfDescrip = fDescrip

If objAuthor = "" Then
tmpAuthor = ""
Else
tmpAuthor = "[" + objAuthor + "] "
End If

If objDate = "" Then
tmpDate = ""
Else
tmpDate = "[" + objDate + "] "
End If

cB1.Value = True
cB2.Value = False
cBoxAuthor.Value = True
cBoxDate.Value = True
cBoxAuthor.Enabled = True
cBoxDate.Enabled = True
Call cBoxAuthor_Click
Call cBoxDate_Click

If objName = "" Then
    Call fNameLabel_Click
    OptionButton1.Enabled = False
Else
    Call objNameLabel_Click
End If

Select Case objName
Case Is = "�������"
    cBoxDate.Caption = "���� ��������� ��������"
    cBoxAuthor.Visible = False
Case Is = "���������"
    cBoxDate.Caption = "���� ���������"
    cBoxAuthor.Caption = "�����"
    If objAuthor = "" Then
    cBoxAuthor.Enabled = False
    End If
Case Is = "�������"
    cBoxDate.Caption = "���� �������"
    cBoxAuthor.Visible = False
Case Is = "������ MS Project"
    cBoxDate.Caption = "���� ���������"
    cBoxAuthor.Caption = "�������"
    If objAuthor = "" Then
    cBoxAuthor.Enabled = False
    End If
Case Is = "������"
    cBoxDate.Caption = "���� ���������"
    cBoxAuthor.Caption = "��������"
    If objAuthor = "" Then
    cBoxAuthor.Enabled = False
    End If
Case Is = "�������"
    cBoxDate.Caption = "���� ��������"
    cBoxAuthor.Caption = "��������"
    If objAuthor = "" Then
        cBoxAuthor.Enabled = False
    Else
        If objAuthor = name Then
            cBoxAuthor.Value = False
        End If
    End If
Case Else
    If objName = "" Then
    cBoxDate.Visible = False
    End If
    If objAuthor = "" Then
    cBoxAuthor.Visible = False
    End If
End Select

End Sub
Private Sub cB1_Click()

If Not tmpFlag Then
    If Not cB1.Value Then
        tmpName = textBox.Text
        textBox.Text = ""
    Else
        textBox.Text = tmpName
    End If
Else
    If Not cB1.Value Then
        tmpfName = textBox.Text
        textBox.Text = ""
    Else
        textBox.Text = tmpfName
    End If
End If

End Sub
Private Sub cB2_Click()

If Not tmpFlag Then
    If Not cB2.Value Then
        tmpDescrip = descripBox.Text
        descripBox.Text = ""
    Else
        descripBox.Text = tmpDescrip
    End If
Else
    If Not cB2.Value Then
        tmpfDescrip = descripBox.Text
        descripBox.Text = ""
    Else
        descripBox.Text = tmpfDescrip
    End If
End If

End Sub

Private Sub cBoxAuthor_Click()

If cB1.Value Then
    If cBoxDate.Value Then
        If cBoxAuthor.Value Then
            textBox.Text = tmpDate + tmpAuthor + tmpTheme
        Else
            textBox.Text = tmpDate + tmpTheme
        End If
    Else
        If cBoxAuthor.Value Then
            textBox.Text = tmpAuthor + tmpTheme
        Else
            textBox.Text = tmpTheme
        End If
    End If
Else
    If cBoxDate.Value Then
        If cBoxAuthor.Value Then
            tmpName = tmpDate + tmpAuthor + tmpTheme
        Else
            tmpName = tmpDate + tmpTheme
        End If
    Else
        If cBoxAuthor.Value Then
            tmpName = tmpAuthor + tmpTheme
        Else
            tmpName = tmpTheme
        End If
    End If
End If

End Sub

Private Sub cBoxDate_Click()

If cB1.Value Then
    If cBoxDate.Value Then
        If cBoxAuthor.Value Then
            textBox.Text = tmpDate + tmpAuthor + tmpTheme
        Else
            textBox.Text = tmpDate + tmpTheme
        End If
    Else
        If cBoxAuthor.Value Then
            textBox.Text = tmpAuthor + tmpTheme
        Else
            textBox.Text = tmpTheme
        End If
    End If
Else
    If cBoxDate.Value Then
        If cBoxAuthor.Value Then
            tmpName = tmpDate + tmpAuthor + tmpTheme
        Else
            tmpName = tmpDate + tmpTheme
        End If
    Else
        If cBoxAuthor.Value Then
            tmpName = tmpAuthor + tmpTheme
        Else
            tmpName = tmpTheme
        End If
    End If
End If

End Sub

Private Sub CommandButton_Click()

If Not tmpFlag Then
    tmpName = textBox.Text
    tmpDescrip = descripBox.Text
Else
    tmpfName = textBox.Text
    tmpfDescrip = descripBox.Text
End If

uF.Hide

End Sub

Public Sub GetParam(ByRef name As String, ByRef descrip As String, _
ByRef flag As Boolean)

If Not tmpFlag Then
    name = tmpName
    descrip = tmpDescrip
Else
    name = tmpfName
    descrip = tmpfDescrip
End If

flag = tmpFlag

End Sub

Private Sub descripBox_Change()

End Sub

Private Sub fNameLabel_Click()

If Not tmpFlag Then
    tmpFlag = True
    
    cBoxAuthor.Enabled = False
    cBoxDate.Enabled = False
    
    If cB1.Value Then
        tmpName = textBox.Text
        textBox.Text = tmpfName
    End If
    
    If cB2.Value Then
        tmpDescrip = descripBox.Text
        descripBox.Text = tmpfDescrip
    End If
End If

OptionButton2.Value = True

End Sub

Private Sub Label2_Click()

Call fNameLabel_Click

End Sub

Private Sub objLabel_Click()

Call objNameLabel_Click

End Sub

Private Sub objNameLabel_Click()

If OptionButton1.Enabled = True Then
    If tmpFlag Then
        tmpFlag = False
        
        If (tmpAuthor = "") Then
            cBoxAuthor.Enabled = False
        Else
            cBoxAuthor.Enabled = True
        End If
        
        cBoxDate.Enabled = True
        
        If cB1.Value Then
            tmpfName = textBox.Text
            textBox.Text = tmpName
        End If
        
        If cB2.Value Then
            tmpfDescrip = descripBox.Text
            descripBox.Text = tmpDescrip
        End If
    End If
    
    OptionButton1.Value = True
End If

End Sub

Private Sub OptionButton1_Click()

Call objNameLabel_Click

End Sub

Private Sub OptionButton2_Click()

Call fNameLabel_Click

End Sub

Private Sub UserForm_Click()

End Sub
