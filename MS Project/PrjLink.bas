Attribute VB_Name = "PrjLink"
Option Explicit

Sub CopyLinkToPrjTask()
    Dim batPath As String
    batPath = "C:\Users\User\Desktop\" '������� ��� .bat ������
    Dim linkerPath As String
    linkerPath = "C:\Users\User\Desktop\������ � ������.exe" '���� � �������
    Dim prefix As String
    prefix = "PRJ-������:     "
    
    Dim tempStr As String
    Dim SelectedItem As String
    Dim projPath As String
    Dim path As String
    Dim linkName As String
    Dim finishDate As String
    Dim resourses As String
    Dim resName As Object
    Dim taskID As Integer
    Dim indent As Boolean
        
    '�������� ����, ��� �������
    If ActiveSelection = 0 Then
        MsgBox ("������ �� �������")
        Exit Sub
    ElseIf ActiveSelection.Tasks.Count > 1 Then
        MsgBox ("������� ������ ������ �������")
        Exit Sub
    End If
    
    On Error GoTo ErrorHandler
    taskID = ActiveSelection.Tasks(1).UniqueID
    finishDate = ActiveSelection.Tasks(1).Finish
    
    If ActiveSelection.Tasks(1).Resources.Count > 0 Then
        resourses = ActiveSelection.Tasks(1).GetField(FieldNameToFieldConstant("�������� ��������"))
    Else
        resourses = ""
    End If
    
    linkName = ActiveProject.name + "\" + ActiveSelection.Tasks(1).name
    projPath = ActiveProject.FullName
    
    '�������� .bat �����
    path$ = batPath + ActiveProject.name + " Task " + CStr(taskID) + ".bat"
    CreateBat path$, linkerPath, projPath, taskID

    SelectedItem = "������ MS Project"
    
    '������ � ���������� ������
        Call uF.Init(SelectedItem, linkName, linkName, resourses, finishDate)
        uF.Show
        Call uF.GetParam(linkName, SelectedItem, indent)

    '�������� ������
    CreateLink path$, prefix + linkName, SelectedItem, indent
Exit Sub

ErrorHandler:
    If Err.Number = 6 Then
        MsgBox ("���������� ��������� ������ �� ������ �������� �������")
    Else
        MsgBox ("����� ������:" + Err.Number + "Chr(10)" + Err.Description)
    End If
    
End Sub
Public Sub CreateLink(ByVal address As String, ByVal name As String, ByVal descrip As String, _
ByVal indent As Boolean)
    Dim hLink As Object
    Dim appWd As Word.Application
    Dim wdDoc As Word.Document
    
    '�������� ���������� Word ��������� ��� �������� ������
    Set appWd = CreateObject("Word.Application")
    Set wdDoc = appWd.Documents.Add
    appWd.Visible = False
    
    Set hLink = wdDoc.Hyperlinks.Add(Anchor:=wdDoc.Range, _
        address:=address, _
        SubAddress:="", _
        ScreenTip:=descrip, _
        TextToDisplay:=name)
    
    '�������������� ������
    hLink.Range.Font.name = "Segoe UI"
    hLink.Range.Font.Size = 10
    hLink.Range.Font.Color = RGB(0, 0, 255)
    
    If Not indent Then
        '�������� ������
        hLink.Range.Copy
    Else
        appWd.Selection.WholeStory
        With appWd.Selection.ParagraphFormat
            .LeftIndent = CentimetersToPoints(0)
            .RightIndent = CentimetersToPoints(0)
            .SpaceBefore = 0
            .SpaceBeforeAuto = False
            .SpaceAfter = 0
            .SpaceAfterAuto = False
            .LineSpacingRule = wdLineSpaceMultiple
            .LineSpacing = LinesToPoints(0.7)
            .Alignment = wdAlignParagraphLeft
            .WidowControl = True
            .KeepWithNext = False
            .KeepTogether = False
            .PageBreakBefore = False
            .NoLineNumber = False
            .Hyphenation = True
            .FirstLineIndent = CentimetersToPoints(0)
            .OutlineLevel = wdOutlineLevelBodyText
            .CharacterUnitLeftIndent = 0
            .CharacterUnitRightIndent = 0
            .CharacterUnitFirstLineIndent = 0
            .LineUnitBefore = 0
            .LineUnitAfter = 0
            .MirrorIndents = False
            .TextboxTightWrap = wdTightNone
            .CollapsedByDefault = False
        End With

        '�������� ������
        appWd.Selection.Range.Copy
        
    End If
    
    wdDoc.Close (False)
    appWd.Quit
    
    'all done, clean up and close
    Set appWd = Nothing
    Set wdDoc = Nothing
    Set hLink = Nothing
    
End Sub

Public Sub CreateBat(ByVal batPath As String, ByVal pathLinker As String, ByVal pathProj As String, _
ByVal task As Integer)

    Dim FSO As Object
    Dim interval As Boolean
    Dim ts
    Set FSO = CreateObject("scripting.filesystemobject")
    
    Set ts = FSO.CreateTextFile(batPath$, True, False)
    ts.Write Trim("@echo off" + Chr(10))
    ts.Write Trim("chcp 1251 >nul" + Chr(10))
    ts.Write Trim("echo File links" + Chr(10))
    ts.Write Trim("start """)
    ts.Write Trim(" File links"" """)
    ts.Write Trim(pathLinker + """ "" ")
    ts.Write Trim("linker"" """ + pathProj)
    ts.Write Trim(""" """ + CStr(task))
    ts.Write Trim(""" " + Chr(10))
    ts.Write Trim("exit")
    ts.Close
  
    Set FSO = Nothing
    Set ts = Nothing

End Sub
 
For Each subT In ActiveProject.Tasks(ParID).OutlineChildren
    subT.Flag1 = True
Next subT
